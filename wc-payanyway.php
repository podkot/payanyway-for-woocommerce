<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

/**
 * Plugin Name: PayAnyWay Payment Gateway
 * Plugin URI: http://code.google.com/p/payanyway/wiki/woocommerce
 * Description: Provides a PayanyWay Payment Gateway.
 * Version: 1.1.8
 * Author: PayAnyWay
 */


/* Add a custom payment class to WC
  ------------------------------------------------------------ */
add_action('plugins_loaded', 'woocommerce_payanyway', 0);
function woocommerce_payanyway()
{
	if (!class_exists('WC_Payment_Gateway'))
		return; // if the WC payment gateway class is not available, do nothing
	if (class_exists('WC_Payanyway'))
		return;

	class WC_Payanyway extends WC_Payment_Gateway
	{
		public function __construct()
		{

			$plugin_dir = plugin_dir_url(__FILE__);

			global $woocommerce;

			$this->id = 'payanyway';
			$this->icon = apply_filters('woocommerce_payanyway_icon', '' . $plugin_dir . 'payanyway.png');
			$this->has_fields = false;

			// Load the settings
			$this->init_form_fields();
			$this->init_settings();

			// Define user set variables
			$this->title = $this->get_option('title');
			$this->MNT_URL = $this->get_option('MNT_URL');
			$this->MNT_ID = $this->get_option('MNT_ID');
			$this->MNT_DATAINTEGRITY_CODE = $this->get_option('MNT_DATAINTEGRITY_CODE');
			$this->MNT_TEST_MODE = $this->get_option('MNT_TEST_MODE');
			$this->debug = $this->get_option('debug');
			$this->description = $this->get_option('description');
			$this->instructions = $this->get_option('instructions');

			// Logs
			if ($this->debug == 'yes') {
				$this->log = new WC_Logger();
			}

			// Actions
			add_action('woocommerce_receipt_payanyway', array($this, 'receipt_page'));

			// Save options
			add_action('woocommerce_update_options_payment_gateways_payanyway', array($this, 'process_admin_options'));

			// Payment listener/API hook
			add_action('woocommerce_api_wc_payanyway', array($this, 'check_assistant_response'));

			if (!$this->is_valid_for_use()) {
				$this->enabled = false;
			}
		}

		/**
		 * Check if this gateway is enabled and available in the user's country
		 */
		function is_valid_for_use()
		{
			if (!in_array(get_option('woocommerce_currency'), array('RUB'))) {
				return false;
			}

			return true;
		}

		/**
		 * Admin Panel Options
		 * - Options for bits like 'title' and availability on a country-by-country basis
		 *
		 * @since 0.1
		 **/
		public function admin_options()
		{
			?>
			<h3><?php _e('PayAnyWay', 'woocommerce'); ?></h3>
			<p><?php _e('Настройка приема электронных платежей через PayAnyWay.', 'woocommerce'); ?></p>

			<?php if ($this->is_valid_for_use()) : ?>

			<table class="form-table">

				<?php
				// Generate the HTML For the settings form.
				$this->generate_settings_html();
				?>
			</table><!--/.form-table-->

		<?php else : ?>
			<div class="inline error"><p>
					<strong><?php _e('Шлюз отключен', 'woocommerce'); ?></strong>: <?php _e('PayAnyWay не поддерживает валюты Вашего магазина.', 'woocommerce'); ?>
				</p></div>
		<?php
		endif;

		} // End admin_options()

		/**
		 * Initialise Gateway Settings Form Fields
		 *
		 * @access public
		 * @return void
		 */
		function init_form_fields()
		{
			$this->form_fields = array(
				'enabled'                => array(
					'title'   => __('Включить/Выключить', 'woocommerce'),
					'type'    => 'checkbox',
					'label'   => __('Включен', 'woocommerce'),
					'default' => 'yes'
				),
				'title'                  => array(
					'title'       => __('Название', 'woocommerce'),
					'type'        => 'text',
					'description' => __('Это название, которое пользователь видит во время проверки.', 'woocommerce'),
					'default'     => __('PayAnyWay', 'woocommerce')
				),
				'MNT_ID'                 => array(
					'title'       => __('Номер счета', 'woocommerce'),
					'type'        => 'text',
					'description' => __('Пожалуйста введите Номер счета.<br/><b>Внимание!</b>Номер расширенного счета на demo.moneta.ru и в рабочем аккаунте PayAnyWay отличаются.', 'woocommerce'),
					'default'     => '99999999'
				),
				'MNT_DATAINTEGRITY_CODE' => array(
					'title'       => __('Код проверки целостности данных', 'woocommerce'),
					'type'        => 'text',
					'description' => __('Пожалуйста введите Код проверки целостности данных, указанный в настройках расширенного счета', 'woocommerce'),
					'default'     => '******'
				),
				'MNT_URL'                => array(
					'title'       => __('URL сервера оплаты', 'woocommerce'),
					'type'        => 'select',
					'options'     => array(
						'demo.moneta.ru'   => 'demo.moneta.ru',
						'www.payanyway.ru' => 'www.payanyway.ru'
					),
					'description' => __('Пожалуйста выберите URL сервера оплаты.', 'woocommerce'),
					'default'     => ''
				),
				'MNT_TEST_MODE'          => array(
					'title'       => __('Тестовый режим', 'woocommerce'),
					'type'        => 'checkbox',
					'label'       => __('Включен', 'woocommerce'),
					'description' => __('В этом режиме плата за товар не снимается.', 'woocommerce'),
					'default'     => 'no'
				),
				'debug'                  => array(
					'title'   => __('Debug', 'woocommerce'),
					'type'    => 'checkbox',
					'label'   => __('Включить логирование (<code>woocommerce/logs/payanyway.txt</code>)', 'woocommerce'),
					'default' => 'no'
				),
				'description'            => array(
					'title'       => __('Description', 'woocommerce'),
					'type'        => 'textarea',
					'description' => __('Описанием метода оплаты которое клиент будет видеть на вашем сайте.', 'woocommerce'),
					'default'     => 'Оплата с помощью payanyway.'
				),
				'instructions'           => array(
					'title'       => __('Instructions', 'woocommerce'),
					'type'        => 'textarea',
					'description' => __('Инструкции, которые будут добавлены на страницу благодарностей.', 'woocommerce'),
					'default'     => 'Оплата с помощью payanyway.'
				)
			);
		}

		/**
		 * Дополнительная информация в форме выбора способа оплаты
		 **/
		function payment_fields()
		{
			if ($this->description) {
				echo wpautop(wptexturize($this->description));
			}
		}

		/**
		 * Process the payment and return the result
		 **/
		function process_payment($order_id)
		{
			$order = new WC_Order($order_id);

			return array(
				'result'   => 'success',
				'redirect' => add_query_arg('order', $order->id, add_query_arg('key', $order->order_key, get_permalink(woocommerce_get_page_id('pay'))))
			);
		}

		/**
		 * Форма оплаты
		 **/
		function receipt_page($order_id)
		{
			echo '<p>' . __('Спасибо за Ваш заказ, пожалуйста, нажмите кнопку ниже, чтобы заплатить.', 'woocommerce') . '</p>';

			$order = new WC_Order($order_id);

			$amount = number_format($order->order_total, 2, '.', '');
			$test_mode = ($this->MNT_TEST_MODE == 'yes') ? 1 : 0;
			$currency = get_woocommerce_currency();
			if ($currency == 'RUR') $currency = 'RUB';
			$signature = md5($this->MNT_ID . $order_id . $amount . $currency . $test_mode . $this->MNT_DATAINTEGRITY_CODE);

			$args = array(
				'MNT_ID'             => $this->MNT_ID,
				'MNT_AMOUNT'         => $amount,
				'MNT_TRANSACTION_ID' => $order_id,
				'MNT_TEST_MODE'      => $test_mode,
				'MNT_CURRENCY_CODE'  => $currency,
				'MNT_SIGNATURE'      => $signature
			);

			$args_array = array();

			foreach ($args as $key => $value) {
				$args_array[] = '<input type="hidden" name="' . esc_attr($key) . '" value="' . esc_attr($value) . '" />';
			}

			echo '<form action="' . esc_url("https://" . $this->MNT_URL . "/assistant.htm") . '" method="POST" id="payanyway_payment_form">' . "\n" .
				implode("\n", $args_array) .
				'<input type="submit" class="button alt" id="submit_payanyway_payment_form" value="' . __('Оплатить', 'woocommerce') . '" /> <a class="button cancel" href="' . $order->get_cancel_order_url() . '">' . __('Отказаться от оплаты & вернуться в корзину', 'woocommerce') . '</a>' . "\n" .
				'</form>';
		}

		/**
		 * Check payanyway Pay URL validity
		 **/
		function check_assistant_request_is_valid($posted)
		{
			if (isset($posted['MNT_ID']) && isset($posted['MNT_TRANSACTION_ID']) && isset($posted['MNT_OPERATION_ID'])
				&& isset($posted['MNT_AMOUNT']) && isset($posted['MNT_CURRENCY_CODE']) && isset($posted['MNT_TEST_MODE'])
				&& isset($posted['MNT_SIGNATURE'])
			) {
				$signature = md5($posted['MNT_ID'] . $posted['MNT_TRANSACTION_ID'] . $posted['MNT_OPERATION_ID'] . $posted['MNT_AMOUNT'] . $posted['MNT_CURRENCY_CODE'] . $posted['MNT_TEST_MODE'] . $this->MNT_DATAINTEGRITY_CODE);
				if ($posted['MNT_SIGNATURE'] !== $signature) {
					return false;
				}
			} else {
				return false;
			}

			return true;
		}

		/**
		 * Check Response
		 **/
		function check_assistant_response()
		{
			global $woocommerce;

			$_REQUEST = stripslashes_deep($_REQUEST);
			if (isset($_REQUEST['payanyway']) AND $_REQUEST['payanyway'] == 'callback') {
				@ob_clean();

				if ($this->check_assistant_request_is_valid($_REQUEST)) {
					$order = new WC_Order($_REQUEST['MNT_TRANSACTION_ID']);

					// Check order not already completed
					if ($order->status == 'completed') {
						die('FAIL');
					}

					// Payment completed
					$order->add_order_note(__('Платеж успешно завершен.', 'woocommerce'));
					$order->payment_complete();

					// cmb2 deletes some metas. prevent it
					add_filter( 'cmb2_override_meta_save', '__return_false', 10, 4 );
					add_filter( 'cmb2_override_meta_remove', '__return_false', 10, 4 );

					// save order meta
					update_post_meta( $order->id, '_payanyway_request', print_r( $_REQUEST, true ) );
					update_post_meta( $order->id, '_payanyway_operation_id', $_REQUEST[ 'MNT_OPERATION_ID' ] );
					update_post_meta( $order->id, '_payanyway_transaction_id', $_REQUEST[ 'MNT_TRANSACTION_ID' ] );

					die('SUCCESS');
				} else {
					die('FAIL');
				}
			} else if (isset($_REQUEST['payanyway']) AND $_REQUEST['payanyway'] == 'success') {
				// $order  = new WC_Order($_REQUEST['MNT_TRANSACTION_ID']);
				// $status = $order->get_status();

				// if ( $status == 'pending' ) {
				// 	$order->update_status('on-hold', __('Платеж на удержании', 'woocommerce'));
				// }

				$woocommerce->cart->empty_cart();

				wp_redirect($this->get_return_url($order));
				exit;
			} else if (isset($_REQUEST['payanyway']) AND $_REQUEST['payanyway'] == 'fail') {
				$order = new WC_Order($_REQUEST['MNT_TRANSACTION_ID']);
				$order->update_status('failed', __('Платеж не оплачен', 'woocommerce'));

				wp_redirect($order->get_cancel_order_url());
				exit;
			}

		}

	}

	/**
	 * Add the gateway to WooCommerce
	 **/
	function add_payanyway_gateway($methods)
	{
		$methods[] = 'WC_Payanyway';

		return $methods;
	}

	add_filter('woocommerce_payment_gateways', 'add_payanyway_gateway');
}

?>
